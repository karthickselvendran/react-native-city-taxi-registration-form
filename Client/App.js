import * as React from 'react';
import { Text, View, StyleSheet, Alert, TouchableHighlight, ScrollView, Picker } from 'react-native';
import Constants from 'expo-constants';
import { ToggleButton } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { Checkbox } from 'react-native-paper';

import { RadioButton } from 'react-native-paper';

import { TextInput } from 'react-native-paper';
import { Dropdown } from 'react-native-material-dropdown';

import { Headline } from 'react-native-paper';

// You can import from local files
import AssetExample from './components/AssetExample';
import { Button } from 'react-native-paper';


// or any pure javascript modules available in npm

export default class App extends React.Component {
  state = {
    email: '',
    password: '',
    confirmpassword: '',
    fullname: '',
    mobilenumber: '',
    address: '',
    pincode: '',
    Gender: 'Male',
    check: false,
    Location: '',

  };

  login = (email, password, confirmpassword, fullname, mobilenumber, address, pincode, checked, Location, check) => {
    alert('Email: ' + email + 'Password: ' + password + 'Confirm Password: ' + confirmpassword + 'Mobile Number: ' + mobilenumber + 'Fullname:' + fullname + 'Address: ' + address + 'Pincode: ' + pincode + 'Gender: ' + checked + 'Location:' + Location + 'Check: ' + check)
  }

  render() {
    const { Gender } = this.state;
    const { check } = this.state;
    let data = [{
      value: 'Trichy',
    }, {
      value: 'Chennai',
    }, {
      value: 'Bangalore',
    }];

    return (
      <View>
       <Appbar.Header>

            <Appbar.Content

              title="City Taxi Registration"
            />
          </Appbar.Header>
      <ScrollView contentContainerStyle={{ height: 750 }}>

        <View style={styles.container} >
         
          <TouchableHighlight>
            <TextInput
              label='Email'
              testID="input"
              mode="outlined"
              keyboardType='email-address'
              value={this.state.email}
              theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
              onChangeText={email => this.setState({ email })}

            />
          </TouchableHighlight>

          <TextInput

            label='Password'
            testID="input"
            mode="outlined"
            secureTextEntry={true}
            theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
            value={this.state.password}
            onChangeText={password => this.setState({ password })}

          />
          <TextInput
            label='Confirm Password'
            testID="input"
            mode="outlined"
            secureTextEntry={true}
            value={this.state.confirmpassword}

            theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
            onChangeText={confirmpassword => this.setState({ confirmpassword })}

          />
          <TextInput
            label='Full Name'
            testID="input"
            mode="outlined"
            value={this.state.fullname}

            theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
            onChangeText={fullname => this.setState({ fullname })}

          />
          <View style={styles.flex}>
            <Text >Male</Text>
            <RadioButton
              value="Male"
              status={Gender === 'Male' ? 'checked' : 'unchecked'}
              onPress={() => { this.setState({ Gender: 'Male' }); }}
            />
            <Text>Female</Text>
            <RadioButton
              value="Female"
              status={Gender === 'Female' ? 'checked' : 'unchecked'}
              onPress={() => { this.setState({ Gender: 'Female' }); }}
            />
          </View>
          <TextInput
            label='Mobile Number'
            testID="input"
            mode="outlined"
            maxLength={10}

            keyboardType="numeric"
            value={this.state.mobilenumber}

            theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
            onChangeText={mobilenumber => this.setState({ mobilenumber })}

          />
          <TextInput
            label='Address'
            testID="input"
            mode="outlined"
            value={this.state.address}
            multiline={true}
            theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
            onChangeText={address => this.setState({ address })}
          />
          <TextInput
            label='pincode'
            testID="input"
            mode="outlined"
            keyboardType="numeric"
            value={this.state.pincode}

            theme={{ colors: { placeholder: 'grey', background: '#f5f6f5', text: 'grey', primary: '#5d5d5d' } }}
            onChangeText={pincode => this.setState({ pincode })}
          />

          <View style={styles.flex}>
            <Text >City : </Text>

            <Picker
              selectedValue={this.state.Location}
              style={{ height: 50, width: 180 }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ Location: itemValue })
              }>
              <Picker.Item label="Trichy" value="Trichy" />
              <Picker.Item label="Chennai" value="Chennai" />
              <Picker.Item label="Bangalore" value="Bangalore" />
              <Picker.Item label="Pune" value="Pune" />

            </Picker>
          </View>
          <View style={styles.flex}>

            <Checkbox
              status={check ? 'checked' : 'unchecked'}
              onPress={() => { this.setState({ check: !check }); }}
            />

            <Text>Accept Terms and Conditions</Text>
          </View>

          <Button mode="contained" onPress={
            () => {
              fetch('http://192.168.50.188:3001/register1', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  Email: this.state.email,
                  Password: this.state.password,
                  Confirmpassword: this.state.confirmpassword,
                  Fullname: this.state.fullname,
                  Gender: this.state.Gender,
                  Mobilenumber: this.state.mobilenumber,
                  Address: this.state.address,
                  Pincode: this.state.pincode,
                  City: this.state.Location,
                  Check: this.state.check,
                }),
              })
              .then(res => Alert.alert("Successfully Registered"))
              .catch(err => console.warn(err));
            }
          }>
            Register
        </Button>
          <Text>  </Text>
          <Text style={styles.bottom}>Already have an account. Sign in?</Text>

        </View>
      </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    marginTop: 20,
    marginLeft: 0,
    marginRight: 0,
    textAlign: 'center',
  },

  flex: {
    flexDirection: 'row',
    alignItems: 'center',
    contentjustify: 'center',
  },
  bottom: {
    textAlign: "center"
  },

});
